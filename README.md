# `loblib::delegate`

The biggest of all thanks to [Andrei Alexandrescu](http://erdani.org/index.html).  
Without his work, this would not be possible, nor would I even decide to learn C++.

> **DISCLAIMER**  
This software is in its initial stages and has not undergone extensive testing. As such, there are probably some bugs and possibly even inefficiencies. Users are advised to exercise caution, conduct their own testing and report a bug if they encounter one. The latter would be very appreciated. This disclaimer will hopefully be removed soon.

## What is this?

The `loblib::delegate` is a C++ header-only template utility that is meant to provide a superior alternative to `std::function` and company, without sacrificing anything.

## Why superior to `std::function`?

- is `const`, `reference` and `noexcept` correct, i.e. includes qualifiers as a part of the signature (this bug of `std::function` is detailed in N4348, is meant to be fixed by `std::copyable_function` (C++26) and is already fixed by `std::move_only_function`)
- has resizable local buffer, which makes it easy to avoid (or to entirely forbid) allocating on the heap
- allows an easy switch to your custom allocator
- allows in-place construction of the target object, by using `std::in_place_type` (this is also implemented by `std::move_only_function`)
- can be made to accept pointers (and pointer-like objects) to invocables (this can be useful, for example, if you want a safe version of `std::function_ref` or if you already have a pointer to the object you want to call and want to avoid unnecessary dereferencing and copying)
- is more performant: it fixes some bugs that `std::function` has (for example, `std::function` passes built-in types by reference), it allows for reference counting instead of deep-copying, it utilizes gcc's ability to partially inline vtable pointers, it is RTTI-free and it can be exception-free
- is extremely flexible: by changing template arguments one can have different behaviors (behaviors of `std::function`, `std::move_only_function`, and `std::function_ref` are all supported as well as many others that can be tailored to your needs)
- is easy to use: you do not have to remember all the different names like 'function', 'move_only_function', 'function_ref', 'copyable_function' ... and you do not have to remember all
the differences between them because API communicates all the differences to you
- is easily modifiable because of strong separation of concerns (if you change error handling behavior, for example, everything else is guaranteed to work as before)

And all of this without any sacrifices in performance, safety, robustness, readability or ease of use!

## How is this even possible?

It's simple: because of the policy-based design!
For each part of the behavior, you specify a policy. Every combination of policies gives you a different behavior.
Most common behaviors are covered by policies that are provided, but if some of them do not fit your bill, all you have to do is write your own (or modify an existing one); everything else stays the same! And all the policies are small and simple, without any data members, so it usually boils down to writing a few static member functions!

Apart from the custom allocator, `Delegate` has 4 policies:

- **Storage policies** specify what will and how be stored in the local storage.
    - `OwningStorage` will always have its own copy of the target object (either through pointer to free memory or in its local buffer on the stack).
    - `NonOwningStorage` will store a reference to the target object instead of the target object itself only if the target is an lvalue reference or if small object optimization can not be used. Therefore, it is not always non-owning.
    - `RefOnlyStorage` will only ever store references, even if you give it an rvalue reference (this makes it ideal for function parameter types).
    - `NonManagingStorage` will only accept trivially-constructible target objects that can be constructed in its local buffer on the stack. This is ideal when you want to make sure you do not pay for copying/destroying objects that do not need to be copied/destroyed in any special way.

- **Ownership policies** specify the ownership strategy.
    - `LocalOwnership` is the strategy that is always used for target objects that can be constructed locally (i.e. it is used for small object optimization - SOO). If specified as the template argument, target objects will never be constructed on the heap. If some other storage policy is specified, `LocalOwnership` will still be used for SOO. If you want to disable SOO, set `BufferSize` to zero. `NonManagingStorage` and `RefOnlyStorage` work only with this strategy, since they prohibit resource management.  
    - `DeepCopy` will always copy the target object. This can be expensive if the target is expensive to copy and/or is stored on the heap.
    - `ReferenceCounting` will never copy the target object, but will instead increase the reference count. The count is stored together with the target object so that there is no memory overhead or memory fragmentation. The count is thread-safe and lock-free.

- **Acceptance policies** specify which target objects can be accepted.
    - `StandardAccept` resembles the acceptance strategy of `std::function`. Non-copyable invocables and pointers to invocables are not accepted.
    - `PtrAccept` is the same as `StandardAccept` but pointers to invocables are accepted.
    - `NonCopyableAccept` is the same as `StandardAccept` but non-copyable invocables are accepted. This implies that copying is prohibited.
    - `NonCopyablePtrAccept` is the same as `NonCopyableAccept` but pointers to invocables are accepted.

- **Error policies** specify what to do when trying to copy/destroy/access or invoke an invalid `Delegate` object.
    - `CheckNone` performs no checking and therefore no error-handling. This can be used in performance-critical code to have one `if` less, but use with caution. Undefined behavior is guaranteed on invoke, copy and on destruction of a moved-from (null) `Delegate` object!
    - `SkipOnError` performs a check before copy/destroy, and skips the action if a `Delegate` object is not in a valid state. Undefined behavior is still guaranteed when invoking a moved-from (null) `Delegate` object (because, a call cannot be skipped since a return value is expected)!
    - `AssertOnError` performs a check before specified actions and asserts that a `Delegate` object is in a valid state.
    - `ThrowOnError` performs a check before specified actions and throws if a `Delegate` object is in an invalid state. On invoke, `std::bad_function_call()` is thrown and on copy/access `std::logic_error` is thrown. Destructors do not throw.

In addition to those 4 policies, you can specify two additional template arguments:  
- `BufferSize` specifies the size threshold for using SOO: objects bigger than this size will never be constructed locally on the stack. If set to 0, SOO will be disabled. By default, it is equal to `sizeof(void*)`. Make sure you increase this if you are using it for pointers to member functions or other bigger objects!
- `Allocator` specifies allocator to be used for (de)allocation and construction/destruction. It needs to be compatible with `std::allocator_traits`

All the policy template arguments have default policies, so you can always choose to be ignorant.  
And there are also a few predefined type-aliases, so that you can be even more lazy!

## Great! How do I start using it?

The `loblib::delegate` has a very permissive MIT license, so no worries there.  
It has no dependencies other than the standard library, so no worries there.  
It is header-only, so all you have to do is copy-paste. There are actually multiple '.hpp' files, but that is purely for organizational purposes (one could merge them in a single big header file, but that would be impractical).  

Therefore, all you have to do is copy a directory named 'loblib' to your project and include 'delegate.hpp' that is inside. That is all!  

> **NOTE**  
The minimum required C++ version is C++20.  
If you want to run the benchmarks yourself, you will need C++23 and boost.

## Examples, please!

```cpp
#include "loblib/delegate.hpp"
using namespace loblib::delegate;

int some_func(int x) { return 2 * x; }

int main() {

	// you can simply use it like this ...
	Delegate<int(int)> cmd1{some_func};

	// which is equivalent to ...
	Delegate<int(int), OwningStorage, ReferenceCounting, StandardAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd1_eq{some_func};

	// if you want behavior more similar to 'std::function' ...
	StandardDelegate<int(int)> cmd2{some_func};

	// which is equivalent to ...
	Delegate<int(int), OwningStorage, DeepCopy, StandardAccept,
	          ThrowOnError, 2 * sizeof(void*), std::allocator> cmd2_eq{some_func};

	// if you want behavior similar to 'std::move_only_function' ...
	MoveOnlyDelegate<int(int)> cmd3{some_func};

	// which is equivalent to ...
	Delegate<int(int), OwningStorage, DeepCopy, NonCopyableAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd3_eq{some_func};

	// and if you want behavior similar to 'std::function_ref' ...
	DelegateRef<int(int)> cmd4{some_func};

	// which is equivalent to ...
	Delegate<int(int), RefOnlyStorage, LocalOwnership, StandardAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd4_eq{some_func};

	// you can also use each version with prefix 'Sz' that stands for 'Sizeable' ...
	SzDelegate<int(int), 2 * sizeof(void*)> cmd5{some_func};

	// or another example ...
	SzMoveOnlyDelegate<int(int), 64> cmd6{some_func};

	// you can also check if an object is of a 'Delegate' type ...
	static_assert(is_delegate_v<decltype(cmd1)>);

	// and you can check if your invocable object would be stored locally or on the heap!
	static_assert(Delegate<int(int)>::using_soo<decltype(some_func)>);

	return 0;
}
```

```cpp
#include "loblib/delegate.hpp"
using namespace loblib::delegate;

struct StatefulFunctor
{
	long n{};
	long m{};
	StatefulFunctor(long x, long y) : n(x), m(y) {}

	long operator()() && noexcept { return n * m; }
};

// make sure you set the size of the local buffer to avoid allocating on the heap
using F = SzDelegate<long() && noexcept, sizeof(StatefulFunctor)>;

#include <iostream>

int main() {

	// construct `StatefulFunctor` in place
	F cmd{std::in_place_type<StatefulFunctor>, 42, 3};

	// this would not work since 'operator()' has '&&' in its signature
	// std::cout << cmd() << std::endl;

	// this will print 126
	std::cout << std::move(cmd)() << '\n';

	// change the target
	cmd = []() noexcept { return 0L; };
	std::cout << std::move(cmd)() << '\n'; // prints 0

	// set new target by creating a temporary and swapping
	F{StatefulFunctor{42, -1}}.swap(cmd);
	std::cout << std::move(cmd)() << '\n'; // prints -42

	// set to null and check that it is indeed null
	cmd = nullptr;
	std::cout << (cmd == nullptr) << '\n'; // prints 1

	return 0;
}
```

## And you claim I get all of this without sacrificing performance?

The `loblib::delegate` is carefully designed so that the overhead compared to calling the invocable object directly is almost zero. Except for one thing: the type-erasure that lies at the heart of functionality that `std::function` provides makes it hard for compilers to inline the call. Compilers will never inline a call through a function pointer, but they might inline a call through a vtable pointer that they created, using a technique called devirtualization. The `loblib::delegate` gives all the hints to compilers that they are free to devirtualize, but there is no guarantee they will do it. If they do, you can get a truly zero overhead delegate! As of now, gcc is able to do this, although partially and not always. The compilers will most likely improve in the future in this respect, so `loblib::delegate` is expected to be even faster!

> **NOTE**  
Make sure you do **not** have `-fno-devirtualize` among your compiler flags, as disabling devirtualization will degrade performance!

#

Here are a few simple benchmarks performed using [nanobench](https://github.com/martinus/nanobench).  

> **DISCLAIMER**  
This benchmarking is by no means comprehensive and should be used only as a guideline.  
If somebody is willing to do a more complete one, I would be happy to include it here (either through link or directly). You can contact me by the e-mail provided below.  
All the benchmarking was done using g++ 13.2 with `-O3 -std=c++23 -fno-lto -fno-whole-program`. Other compilers were not tested!

### Benchmark1

baseline : calling a stateless lambda N times

```cpp
void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	auto lambda = [](float x) { return x + 3.F; };
	using BaselineType = decltype(lambda);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{lambda};

	bench->run(name,
	           [&]()
	           {
				   float accumulator = 1;
				   for (std::size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |        7,437,921.00 |   20,000,022.00 |   30,006,937.50 |   5,000,007.00 |      1.49 |
| `std_function`       |    27.3% |       27,290,580.00 |  130,000,042.00 |  110,024,673.50 |  40,000,027.00 |      5.46 |
| `p2548_copyable`     |    50.0% |       14,886,477.50 |   90,000,030.00 |   60,013,681.50 |  30,000,015.00 |      2.98 |
| `boost_function`     |    50.0% |       14,886,682.00 |  120,000,030.00 |   60,011,800.50 |  40,000,015.00 |      2.98 |
| `fu2_function`       |    60.0% |       12,405,615.50 |   90,000,027.00 |   50,010,196.00 |  30,000,012.00 |      2.48 |
| `delegate_default`   |    99.9% |        7,442,700.00 |   25,000,038.00 |   30,005,128.00 |   5,000,012.00 |      1.49 |
| `delegate_standard`  |   100.0% |        7,441,347.00 |   25,000,039.00 |   30,005,510.00 |   5,000,012.00 |      1.49 |
| `std_move_only`      |    60.0% |       12,402,365.00 |   80,000,027.00 |   50,011,566.00 |  30,000,012.00 |      2.48 |
| `fu2_unique_function`|    60.0% |       12,403,326.50 |   90,000,027.00 |   50,010,889.00 |  30,000,012.00 |      2.48 |
| `delegate_move_only` |   100.1% |        7,433,037.00 |   25,000,038.00 |   30,006,741.50 |   5,000,012.00 |      1.49 |
| `p2548_function_ref` |    60.0% |       12,401,468.00 |   80,000,027.00 |   50,011,584.00 |  30,000,012.00 |      2.48 |
| `fu2_function_view`  |    60.0% |       12,403,802.00 |   90,000,027.00 |   50,011,626.50 |  30,000,012.00 |      2.48 |
| `delegate_ref`       |   100.0% |        7,438,567.00 |   25,000,038.00 |   30,006,644.00 |   5,000,012.00 |      1.49 |

### Benchmark2

baseline : calling a function pointer N times

```cpp
namespace { int testfunc(int x) { return x * 3; } }

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = float (*)(float);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   float accumulator = 1;
				   for (std::size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       12,398,682.50 |   70,000,027.00 |   50,028,195.50 |  30,000,012.00 |      2.48 |
| `std_function`       |    42.9% |       28,926,701.50 |  150,000,044.00 |  116,687,806.50 |  50,000,029.00 |      5.79 |
| `p2548_copyable`     |    83.3% |       14,884,514.00 |  110,000,030.00 |   60,042,068.50 |  40,000,015.00 |      2.98 |
| `boost_function`     |    71.4% |       17,364,859.50 |  140,000,032.00 |   70,044,238.50 |  50,000,017.00 |      3.47 |
| `fu2_function`       |    71.4% |       17,369,478.00 |  200,000,032.00 |   70,048,498.00 |  60,000,017.00 |      3.48 |
| `delegate_default`   |   100.0% |       12,403,447.00 |  130,000,027.00 |   50,033,042.00 |  50,000,013.00 |      2.48 |
| `delegate_standard`  |    83.2% |       14,905,342.50 |  130,000,032.00 |   60,015,496.50 |  50,000,018.00 |      2.98 |
| `std_move_only`      |    83.2% |       14,906,274.00 |  100,000,032.00 |   60,015,289.00 |  40,000,017.00 |      2.98 |
| `fu2_unique_function`|    71.3% |       17,387,667.00 |  200,000,034.00 |   70,018,972.00 |  60,000,019.00 |      3.48 |
| `delegate_move_only` |    83.2% |       14,907,786.50 |  130,000,032.00 |   60,015,420.50 |  50,000,018.00 |      2.98 |
| `p2548_function_ref` |    83.2% |       14,908,578.50 |  100,000,032.00 |   60,015,223.00 |  40,000,017.00 |      2.98 |
| `fu2_function_view`  |    62.4% |       19,864,906.50 |  110,000,037.00 |   80,019,928.00 |  40,000,022.00 |      3.85 |
| `delegate_ref`       |    99.8% |       12,422,031.00 |  130,000,029.00 |   50,013,596.50 |  50,000,015.00 |      2.49 |

### Benchmark3

baseline : calling a function that has a big object parameter N times

```cpp
namespace {

struct BigObject
{
	char data[64];
	int n{};

	BigObject(int x) : n(x){};
};

int testfunc(BigObject x) { return x.n + 3; }

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = int (*)(BigObject);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   BigObject bo{1};
				   for (std::size_t i = 0; i < N; i++) {
					   bo.n = std::invoke(cmd, bo);
				   }
				   ankerl::nanobench::doNotOptimizeAway(bo);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       17,361,087.50 |  220,000,041.00 |   70,016,845.50 |  30,000,017.00 |      3.48 |
| `std_function`       |    43.8% |       39,644,955.00 |  280,000,063.00 |  159,922,120.00 |  60,000,040.00 |      7.90 |
| `p2548_copyable`     |    44.6% |       38,918,560.50 |  270,000,062.00 |  156,965,882.50 |  50,000,039.00 |      7.79 |
| `boost_function`     |    46.7% |       37,207,959.50 |  300,000,060.00 |  150,035,175.50 |  60,000,037.00 |      7.44 |
| `fu2_function`       |    46.4% |       37,408,390.50 |  360,000,060.00 |  150,875,903.00 |  70,000,037.00 |      7.49 |
| `delegate_default`   |   100.0% |       17,360,891.50 |  240,000,041.00 |   70,017,130.50 |  40,000,017.00 |      3.47 |
| `delegate_standard`  |   100.0% |       17,362,134.50 |  240,000,041.00 |   70,017,233.00 |  40,000,017.00 |      3.47 |
| `std_move_only`      |    45.0% |       38,579,671.50 |  260,000,062.00 |  155,596,078.50 |  50,000,039.00 |      7.71 |
| `fu2_unique_function`|    46.6% |       37,286,943.00 |  360,000,060.00 |  150,363,947.00 |  70,000,037.00 |      7.48 |
| `delegate_move_only` |   100.0% |       17,361,868.00 |  240,000,041.00 |   70,016,497.50 |  40,000,017.00 |      3.47 |
| `p2548_function_ref` |    45.2% |       38,430,738.00 |  260,000,061.00 |  155,065,560.00 |  50,000,038.00 |      7.68 |
| `fu2_function_view`  |    44.6% |       38,964,101.00 |  270,000,062.00 |  157,124,257.50 |  50,000,039.00 |      7.79 |
| `delegate_ref`       |   100.0% |       17,362,014.00 |  240,000,041.00 |   70,017,257.00 |  40,000,017.00 |      3.47 |

### Benchmark4

baseline : calling N stateful functors

```cpp
namespace
{

struct StatefulFunctor
{
	int n = 3;
	int a[15];
	int operator()(int x) { return x + n; }
};

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	StatefulFunctor stateful_functor{};
	using BaselineType = StatefulFunctor;

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;

	auto ftors = std::vector<Ftor>{};
	ftors.reserve(N);

	for (std::size_t i = 0; i < N; ++i) {
		ftors.emplace_back(stateful_functor);
	}

	bench->run(name,
	           [&]()
	           {
				   int accumulator = 1;
				   for (auto& ftor : ftors) {
					   accumulator = std::invoke(ftor, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       31,200,265.00 |   40,000,049.00 |  125,370,491.00 |  10,000,032.00 |      6.44 |
| `std_function`       |    70.4% |       44,319,351.00 |  140,000,064.00 |  177,740,759.50 |  40,000,047.00 |      9.13 |
| `p2548_copyable`     |    67.3% |       46,369,190.50 |  120,000,066.00 |  186,002,624.50 |  30,000,049.00 |      9.37 |
| `boost_function`     |    64.6% |       48,312,040.50 |  150,000,066.00 |  194,006,599.00 |  40,000,049.00 |      9.72 |
| `fu2_function`       |    68.3% |       45,653,136.50 |  120,000,064.00 |  183,280,437.00 |  30,000,047.00 |      9.21 |
| `delegate_default`   |    79.3% |       39,364,034.00 |  110,000,057.00 |  157,942,021.50 |  30,000,041.00 |      8.13 |
| `delegate_standard`  |    74.3% |       41,985,343.50 |  110,000,059.00 |  168,639,454.00 |  30,000,043.00 |      8.40 |
| `std_move_only`      |    63.4% |       49,234,025.50 |  110,000,067.00 |  197,762,400.50 |  30,000,050.00 |      9.85 |
| `fu2_unique_function`|    68.1% |       45,812,704.00 |  120,000,064.00 |  184,006,580.00 |  30,000,047.00 |      9.17 |
| `delegate_move_only` |    78.3% |       39,829,534.50 |  110,000,057.00 |  159,898,287.00 |  30,000,041.00 |      7.99 |
| `p2548_function_ref` |   181.0% |       17,234,375.00 |  100,000,035.00 |   69,296,714.00 |  30,000,018.00 |      3.45 |
| `fu2_function_view`  |   199.5% |       15,639,846.50 |  120,000,034.00 |   62,868,391.50 |  30,000,017.00 |      3.13 |
| `delegate_ref`       |   292.6% |       10,664,322.50 |  110,000,028.00 |   42,859,458.50 |  30,000,012.00 |      2.13 |

And if the local buffer size is increased to 64 bytes:

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `fu2_function64`     |    74.4% |       41,958,979.50 |  200,000,060.00 |  168,513,447.50 |  50,000,043.00 |      8.39 |
| `sz_delegate64`      |   101.0% |       30,946,239.50 |  100,000,048.00 |  124,289,162.50 |  30,000,032.00 |      6.19 |

<br/><br/>
If you want to run the benchmarks yourself, they are in the directory 'benchmarks'.  

For example, to compile and run the first benchmark:
```bash
cd ./benchmarks
make run_benchmark1.out && ./run_benchmark1.out
# ...
make clean
```

> **NOTE**  
To provide a fair benchmarking, all the candidates are compiled separately and then linked.  
Link-time and whole-program optimizations are turned off with `-fno-lto -fno-whole-program`.

## Contribute

There are a lot of things that can be improved/added to the project. Most notably, unit tests and more complete benchmarks.  
Pull requests and new issues are very welcome!  
You can contact me by e-mail: <lobel.krivic@proton.me>.  

#

I work for no company and have no sponsors.  
If you like my work, consider buying me a cup of coffee...  

<a href="https://buymeacoffee.com/freedom1947" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>  

<a href="https://pay-link.s3.us-west-2.amazonaws.com/index.html?uid=b00668d46dde4925"><img src="https://web.archive.org/web/20130429152428if_/http://www.ecogex.com/bitcoin/pack/ribbonDonateBitcoin.png" height="41" width="174"></a>

## License
[MIT](https://gitlab.com/loblib/delegate/-/blob/master/LICENSE)

