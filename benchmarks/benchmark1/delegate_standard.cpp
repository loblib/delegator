#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::StandardDelegate<float(float) const>;
}

#define BENCH_NAME delegate_standard
#include "baseline.cpp"
#undef BENCH_NAME

