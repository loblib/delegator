#include <functional>

namespace
{
using F = std::function<float(float)>;
}

#define BENCH_NAME std_function
#include "baseline.cpp"
#undef BENCH_NAME
