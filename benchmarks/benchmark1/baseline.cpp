#include <nanobench.hpp>

#include <functional>

#ifndef BENCH_NAME
#	define BENCH_NAME baseline
using F = void;
#endif

// baseline: calling a stateless lambda N times

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	auto lambda = [](float x) { return x + 3.F; };
	using BaselineType = decltype(lambda);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{lambda};

	bench->run(name,
	           [&]()
	           {
				   float accumulator = 1;
				   for (std::size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
