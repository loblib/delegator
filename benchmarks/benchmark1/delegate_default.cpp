#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::Delegate<float(float) const>;
}

#define BENCH_NAME delegate_default
#include "baseline.cpp"
#undef BENCH_NAME
