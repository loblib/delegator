#include <loblib/delegate.hpp>

namespace
{
struct BigObject;
using F = loblib::delegate::DelegateRef<int(BigObject&) const>;
}

#define BENCH_NAME delegate_ref
#include "baseline.cpp"
#undef BENCH_NAME

