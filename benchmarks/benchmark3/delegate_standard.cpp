#include <loblib/delegate.hpp>

namespace
{
struct BigObject;
using F = loblib::delegate::StandardDelegate<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegate_standard
#include "baseline.cpp"
#undef BENCH_NAME

