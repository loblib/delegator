#include <loblib/delegate.hpp>

namespace
{
struct BigObject;
using F = loblib::delegate::MoveOnlyDelegate<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegate_move_only
#include "baseline.cpp"
#undef BENCH_NAME

