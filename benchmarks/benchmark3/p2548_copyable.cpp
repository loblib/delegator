#include <p2548_copyable_function.hpp>

namespace
{
struct BigObject;
using F = p2548::copyable_function<int(BigObject&) const>;
}

#define BENCH_NAME p2548_copyable
#include "baseline.cpp"
#undef BENCH_NAME
