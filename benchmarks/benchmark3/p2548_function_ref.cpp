#include <p2548_function_ref.hpp>

namespace
{
struct BigObject;
using F = p2548::function_ref<int(BigObject&) const>;
}

#define BENCH_NAME p2548_function_ref
#include "baseline.cpp"
#undef BENCH_NAME
