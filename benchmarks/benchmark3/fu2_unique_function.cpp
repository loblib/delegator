#include <function2.hpp>

namespace
{
struct BigObject;
using F = fu2::function<int(BigObject&) const>;
} // namespace

#define BENCH_NAME fu2_unique_function
#include "baseline.cpp"
#undef BENCH_NAME

