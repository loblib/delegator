#include <nanobench.hpp>

#include <functional>

#ifndef BENCH_NAME
#	define BENCH_NAME baseline
using F = void;
#endif

// baseline: calling a function that has a big object parameter N times

namespace
{

struct BigObject
{
	char data[64];
	int n{};

	BigObject(int x) : n(x){};
};

int testfunc(BigObject x) { return x.n + 3; }

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = int (*)(BigObject);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   BigObject bo{1};
				   for (std::size_t i = 0; i < N; i++) {
					   bo.n = std::invoke(cmd, bo);
				   }
				   ankerl::nanobench::doNotOptimizeAway(bo);
			   });
}
