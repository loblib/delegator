#include <loblib/delegate.hpp>

namespace
{
struct BigObject;
using F = loblib::delegate::Delegate<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegate_default
#include "baseline.cpp"
#undef BENCH_NAME
