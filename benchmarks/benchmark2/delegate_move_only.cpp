#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::MoveOnlyDelegate<float(float) const>;
}

#define BENCH_NAME delegate_move_only
#include "baseline.cpp"
#undef BENCH_NAME

