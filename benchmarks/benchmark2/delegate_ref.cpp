#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::DelegateRef<float(float) const>;
}

#define BENCH_NAME delegate_ref
#include "baseline.cpp"
#undef BENCH_NAME

