#include <nanobench.hpp>

#include <functional>

#ifndef BENCH_NAME
#	define BENCH_NAME baseline
using F = void;
#endif

// baseline: calling a function pointer N times

namespace
{

float testfunc(float x) { return x + 3.F; }

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = float (*)(float);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   float accumulator = 1;
				   for (std::size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
