#include <nanobench.hpp>

#include <vector>
#include <functional>

#ifndef BENCH_NAME
#	define BENCH_NAME baseline
using F = void;
#endif

// baseline: calling N stateful functors

namespace
{

struct StatefulFunctor
{
	int n = 3;
	int a[15];
	int operator()(int x) { return x + n; }
};

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	StatefulFunctor stateful_functor{};
	using BaselineType = StatefulFunctor;

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;

	auto ftors = std::vector<Ftor>{};
	ftors.reserve(N);

	for (std::size_t i = 0; i < N; ++i) {
		ftors.emplace_back(stateful_functor);
	}

	bench->run(name,
	           [&]()
	           {
				   int accumulator = 1;
				   for (auto& ftor : ftors) {
					   accumulator = std::invoke(ftor, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
