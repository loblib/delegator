#include <function2.hpp>

namespace
{
using F = fu2::function<int(int)>;
// using F = fu2::function_base<true, true, fu2::capacity_fixed<64>, false, false, int(int)>;
} // namespace

#define BENCH_NAME fu2_function
#include "baseline.cpp"
#undef BENCH_NAME

