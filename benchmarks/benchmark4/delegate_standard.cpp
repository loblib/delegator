#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::StandardDelegate<int(int)>;
// using F = loblib::delegate::SzDelegate<int(int), 64>;
} // namespace

#define BENCH_NAME delegate_standard
#include "baseline.cpp"
#undef BENCH_NAME

