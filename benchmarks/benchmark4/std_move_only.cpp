#include <functional>

namespace
{
using F = std::move_only_function<int(int)>;
}

#define BENCH_NAME std_move_only
#include "baseline.cpp"
#undef BENCH_NAME

