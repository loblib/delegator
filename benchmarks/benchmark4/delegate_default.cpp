#include <loblib/delegate.hpp>

namespace
{
using F = loblib::delegate::Delegate<int(int)>;
}

#define BENCH_NAME delegate_default
#include "baseline.cpp"
#undef BENCH_NAME
