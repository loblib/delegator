#ifndef LOBLIB_DELEGATE_ERROR_POLICIES
#define LOBLIB_DELEGATE_ERROR_POLICIES

#include <cassert>
#include <functional>

#include "object_manager/error_policies.hpp"

///////////////////////////////////////////////////////

namespace loblib::delegate
{

namespace internal
{

using namespace object_manager::error_policies;

} // namespace internal

namespace error_policies
{

class CheckNone : protected internal::CheckNone
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_call = false;

	static void on_invoke() noexcept {}

	CheckNone() = default;
	~CheckNone() = default;
};

class SkipOnError : protected internal::SkipOnError
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_call = false;

	static void on_invoke() noexcept {}

	SkipOnError() = default;
	~SkipOnError() = default;
};

class AssertOnError : protected internal::AssertOnError
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_call = true;

	static void on_invoke() noexcept { assert(false); }

	AssertOnError() = default;
	~AssertOnError() = default;
};

class ThrowOnError : protected internal::ThrowOnError
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_call = true;

	static void on_invoke() { throw std::bad_function_call(); }

	ThrowOnError() = default;
	~ThrowOnError() = default;
};

} // namespace error_policies
} // namespace loblib::delegate

///////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE_ERROR_POLICIES
