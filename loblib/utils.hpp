#ifndef LOBLIB_DELEGATE_UTILS
#define LOBLIB_DELEGATE_UTILS

#include <cstddef>
#include <type_traits>
#include <utility>
#include <memory>

///////////////////////////////////////////////////////

namespace loblib::delegate
{

namespace internal
{

template <typename T>
inline consteval bool make_false() {
	return false;
}

class UndefinedClass;
inline constexpr std::size_t MPtrSize = sizeof(void(UndefinedClass::*)());

// return the smallest multiple of `Alignment` that is >= `N` (add padding to `N`)
template <std::size_t Alignment>
inline consteval std::size_t AddPaddingToSize(std::size_t N) {
	if (N == 0) { return 0; }
	return Alignment * (((N - 1) / Alignment) + 1);
}

template <typename T>
using RefNonTrivials = std::conditional_t<std::is_scalar_v<T>, T, T&&>;

// change to 'is_invocable_r', 'is_nothrow_invocable_r' that are available since cpp23
template <typename T, typename ReturnType, bool Noexcept, typename... ParamTypes>
using is_invocable = std::conditional_t <
							Noexcept,
							std::is_nothrow_invocable<T, ParamTypes...>,
							std::is_invocable<T, ParamTypes...>
						>;

template <typename T>
inline constexpr bool is_dereferencable_v =
	requires { typename std::pointer_traits<T>::element_type; };

template <typename T>
using is_dereferencable = std::bool_constant<is_dereferencable_v<T>>;

template <typename T>
requires is_dereferencable_v<T>
using Dereferenced = typename std::pointer_traits<T>::element_type;

///////////////////////////////////////////////////////

template <typename T>
inline constexpr bool is_function_pointer_v =
	std::is_function_v<typename std::remove_pointer_t<T>> && std::is_pointer_v<T>;

template <typename T>
using is_function_pointer = std::bool_constant<is_function_pointer_v<T>>;

template <typename>
inline constexpr bool is_in_place_type_v = false;

template <typename T>
inline constexpr bool is_in_place_type_v<std::in_place_type_t<T>> = true;

template <typename T>
using is_in_place_type = std::bool_constant<is_in_place_type_v<T>>;

} // namespace internal
} // namespace loblib::delegate

///////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE_UTILS
