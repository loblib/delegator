#ifndef LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES
#define LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES

#include <type_traits>
#include <utility>
#include <memory>
#include <atomic>

#include "utils.hpp"

///////////////////////////////////////////////////////

namespace loblib::object_manager
{

namespace internal
{

template <typename T, template <typename> class Allocator>
class ReferenceCountingWrapper
{
  public:
	using AllocatorType = Allocator<ReferenceCountingWrapper>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;

  private:
	std::atomic<int> _count;
	T _obj;

  public:
	template <typename... Args>
	[[nodiscard]]
	static ReferenceCountingWrapper*
	create_new(AllocatorType& alloc, Args&&... args) {
		ReferenceCountingWrapper* ptr = AllocatorTraits::allocate(alloc, 1);
		ScopeGuard allocation_guard{
			[&alloc, &ptr]() { AllocatorTraits::deallocate(alloc, ptr, 1); }};

		AllocatorTraits::construct(alloc, ptr, std::forward<Args>(args)...);
		allocation_guard.disarm();

		return ptr;
	}

	[[nodiscard]]
	static T& access(ReferenceCountingWrapper* target) noexcept {
		return target->_obj;
	}

	[[nodiscard]]
	static ReferenceCountingWrapper* copy(ReferenceCountingWrapper* target) noexcept {
		(target->_count).fetch_add(1, std::memory_order_relaxed);
		return target;
	}

	static void destroy(AllocatorType& alloc,
	                    ReferenceCountingWrapper* target) noexcept {
		int count = (target->_count).fetch_sub(1, std::memory_order_release) - 1;
		if (count == 0) {
			std::atomic_thread_fence(std::memory_order_acquire);
			AllocatorTraits::destroy(alloc, target);
			AllocatorTraits::deallocate(alloc, target, 1);
		}
	}

	// ctor/dtor cannot be private because std::construct_at/std::destroy_at is
	// not friend even if AllocatorTraits is, but that is not a problem; if a
	// client creates/destroys using ctor/dtor, reference counting won't be used
	// but nothing will be broken

	template <typename... Args>
	ReferenceCountingWrapper(Args&&... args)
		: _count(1), _obj(std::forward<Args>(args)...) {}

	~ReferenceCountingWrapper() = default;
};

} // namespace internal

///////////////////////////////////////////////////////

namespace ownership_policies
{

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class DeepCopy;

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class ReferenceCounting;

template <template <typename, template <typename> class> class,
          template <typename, template <typename> class> class>
class is_same_ownership_policy : public std::false_type
{};

template <template <typename, template <typename> class> class OP>
class is_same_ownership_policy<OP, OP> : public std::true_type
{};

template <template <typename, template <typename> class> class OP1,
          template <typename, template <typename> class> class OP2>
inline constexpr bool is_same_ownership_policy_v =
	is_same_ownership_policy<OP1, OP2>::value;

///////////////////////////////////////////////////////

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class DeepCopy
{
  public:
	using AllocatorType = Allocator<TargetType>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = TargetType*;

	template <typename... Args>
	static constexpr bool can_nothrow_construct = false;

	static constexpr bool can_nothrow_copy = false;

  protected:
	DeepCopy() = default;
	~DeepCopy() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept {
		return *stored;
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args) {
		static_assert(std::is_constructible_v<std::decay_t<TargetType>, Args...>,
		              "Target object must be constructible");

		*target = AllocatorTraits::allocate(alloc, 1);
		internal::ScopeGuard allocation_guard{
			[&alloc, &target]() { AllocatorTraits::deallocate(alloc, *target, 1); }};

		AllocatorTraits::construct(alloc, *target, std::forward<Args>(args)...);
		allocation_guard.disarm();
	}

	static void copy(AllocatorType& alloc, const StoredType& source,
	                 StoredType* destination) {
		static_assert(std::is_copy_constructible_v<std::decay_t<TargetType>>,
		              "Target object must be copy-constructible");

		create(alloc, destination, *source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept {
		AllocatorTraits::destroy(alloc, *target);
		AllocatorTraits::deallocate(alloc, *target, 1);
	}

	DeepCopy(const ReferenceCounting<TargetType, Allocator>&) = delete;
};

///////////////////////////////////////////////////////

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class ReferenceCounting
{
  private:
	using WrappedTarget = internal::ReferenceCountingWrapper<TargetType, Allocator>;

  public:
	using AllocatorType = WrappedTarget::AllocatorType;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = WrappedTarget*;

	template <typename... Args>
	static constexpr bool can_nothrow_construct = false;

	static constexpr bool can_nothrow_copy = false;

  protected:
	ReferenceCounting() = default;
	~ReferenceCounting() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept {
		return WrappedTarget::access(stored);
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args) {
		static_assert(std::is_constructible_v<std::decay_t<TargetType>, Args...>,
		              "Target object must be constructible");

		*target = WrappedTarget::create_new(alloc, std::forward<Args>(args)...);
	}

	static void copy(AllocatorType&, const StoredType& source,
	                 StoredType* destination) noexcept {
		// not necessary for `TargetType` to be copy-constructible

		*destination = WrappedTarget::copy(source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept {
		WrappedTarget::destroy(alloc, *target);
	}

	ReferenceCounting(const DeepCopy<TargetType, Allocator>&) {}
};

///////////////////////////////////////////////////////

} // namespace ownership_policies
} // namespace loblib::object_manager

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES
