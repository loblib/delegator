#ifndef LOBLIB_OBJECT_MANAGER
#define LOBLIB_OBJECT_MANAGER

#include <cstddef>
#include <type_traits>
#include <utility>
#include <memory>
#include <exception>

#include "utils.hpp"
#include "error_policies.hpp"
#include "ownership_policies.hpp"

///////////////////////////////////////////////////////

namespace loblib::object_manager
{

using namespace error_policies;
using namespace ownership_policies;

namespace internal
{

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class LocalOwnership
{
  public:
	using AllocatorType = Allocator<TargetType>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = std::decay_t<TargetType>;

	template <typename... Args>
	static constexpr bool can_nothrow_construct =
		std::is_nothrow_constructible_v<StoredType, Args...>;

	static constexpr bool can_nothrow_copy =
		can_nothrow_construct<const StoredType&>;

  protected:
	LocalOwnership() = default;
	~LocalOwnership() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept
	{
		return stored;
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args)
	noexcept(can_nothrow_construct<Args...>)
	{
		AllocatorTraits::construct(alloc, target, std::forward<Args>(args)...);
	}

	static void
	copy(AllocatorType& alloc, const StoredType& source, StoredType* destination)
	noexcept(can_nothrow_copy)
	{
		AllocatorTraits::construct(alloc, destination, source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept
	{
		AllocatorTraits::destroy(alloc, target);
	}
};

template 
<
	typename TargetType, 
	template <typename, template <typename> class> 
	class OwnershipTemplate,
	std::size_t BufferSize, 
	template <typename> class Allocator
>
class ManagerHelper
{
  protected:
	using MaxBuffer = internal::Any<BufferSize>;

	static constexpr bool using_soo =
		MaxBuffer::template can_construct<TargetType>();

  public:
	using OwnershipPolicy =
		std::conditional_t
		<
			using_soo,
			internal::LocalOwnership<TargetType, Allocator>,
			OwnershipTemplate<TargetType, Allocator>
		>;

	using AllocatorType = OwnershipPolicy::AllocatorType;
	using StoredType = OwnershipPolicy::StoredType;
	using Buffer = internal::Any<sizeof(StoredType)>;

	template <typename... Args>
	static constexpr bool is_constructible =
		Buffer::template can_construct<StoredType>()
		&& std::is_constructible_v<std::decay_t<TargetType>, Args...>;

	static constexpr bool is_copy_constructible =
		Buffer::template can_construct<StoredType>()
		&& std::is_copy_constructible_v<std::decay_t<TargetType>>;
};

} // namespace internal

template 
<
	typename TargetType, 
	template <typename, template <typename> class> 
	class OwnershipTemplate = ReferenceCounting,
	class ErrorPolicy = AssertOnError,
	std::size_t BufferSize = sizeof(void*),
	template <typename> class Allocator = std::allocator
>
class ObjectManager
	: private internal::ManagerHelper <
					TargetType,
					OwnershipTemplate,
					BufferSize,
					Allocator
				>
	, private internal::ManagerHelper <
					TargetType,
					OwnershipTemplate,
					BufferSize,
					Allocator
				>::AllocatorType
	, public internal::ManagerHelper <
					TargetType,
					OwnershipTemplate,
					BufferSize,
					Allocator
				>::OwnershipPolicy
	, public ErrorPolicy
{
  private:
	using ManagerHelper = internal::ManagerHelper <
					TargetType,
					OwnershipTemplate,
					BufferSize,
					Allocator
				>;

	using typename ManagerHelper::OwnershipPolicy;
	using typename ManagerHelper::AllocatorType;
	using typename ManagerHelper::StoredType;
	using typename ManagerHelper::Buffer;

	Buffer _local_buffer;

	////////////////////////

	[[nodiscard]]
	AllocatorType& m_allocator() noexcept {
		return static_cast<AllocatorType&>(*this);
	}

	[[nodiscard]]
	const StoredType& m_stored_object() const noexcept {
		return _local_buffer.template interpret_as<const StoredType&>();
	}

	[[nodiscard]]
	StoredType* m_stored_object_ptr() noexcept {
		return static_cast<StoredType*>(_local_buffer.address());
	}

	static consteval bool m_can_nothrow_access() noexcept {
		if constexpr (ErrorPolicy::check_before_access) {
			return noexcept(ErrorPolicy::on_access());
		}
		return true;
	}

	////////////////////////

  public:
	[[nodiscard]]
	const TargetType& access() const& 
	noexcept(m_can_nothrow_access())
	{
		if constexpr (ErrorPolicy::check_before_access) {
			if (is_invalid()) {
				ErrorPolicy::on_access();
				std::terminate();
			}
		}

		return OwnershipPolicy::get_target(m_stored_object());
	}

	[[nodiscard]]
	TargetType& access() &
	noexcept(m_can_nothrow_access())
	{
		return const_cast<TargetType&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	[[nodiscard]]
	const TargetType&& access() const&& 
	noexcept(m_can_nothrow_access())
	{
		return static_cast<const TargetType&&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	[[nodiscard]]
	TargetType&& access() && 
	noexcept(m_can_nothrow_access())
	{
		return const_cast<TargetType&&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	////////////////////////

	using ManagerHelper::using_soo;

	// general constructor
	template <typename... Args>
	requires(!((std::is_same_v<ObjectManager, std::remove_cvref_t<Args>> && ...)
	           && (sizeof...(Args) == 1)))
	ObjectManager(Args&&... args)
	noexcept(OwnershipPolicy::template can_nothrow_construct<Args...>)
	requires(ManagerHelper::template is_constructible<Args...>)
		: AllocatorType(), OwnershipPolicy(), ErrorPolicy()
	{
		static_assert(sizeof(typename ManagerHelper::MaxBuffer) >= sizeof(Buffer),
		              "Buffer size is too small for the specified target "
		              "object and the ownership policy!");

		OwnershipPolicy::create(m_allocator(), m_stored_object_ptr(),
		                        std::forward<Args>(args)...);
	}

	// destructor
	~ObjectManager()
	{
		if constexpr (ErrorPolicy::check_before_destroy) {
			if (is_invalid()) {
				ErrorPolicy::on_destroy();
				return;
			}
		}

		OwnershipPolicy::destroy(m_allocator(), m_stored_object_ptr());
	}

	// copy constructor
	ObjectManager(const ObjectManager& other)
	noexcept(noexcept(ErrorPolicy::on_copy()) 
	         && OwnershipPolicy::can_nothrow_copy)
	requires(ManagerHelper::is_copy_constructible)
		: AllocatorType(other), OwnershipPolicy(other), ErrorPolicy(other)
	{
		if constexpr (ErrorPolicy::check_before_copy) {
			if (other.is_invalid()) {
				ErrorPolicy::on_copy();
				invalidate();
				return;
			}
		}

		OwnershipPolicy::copy(m_allocator(), other.m_stored_object(),
		                      m_stored_object_ptr());
	}

	// move constructor
	ObjectManager(ObjectManager&& other) noexcept
		: AllocatorType(std::move(other))
		, OwnershipPolicy(std::move(other))
		, ErrorPolicy(std::move(other))
		, _local_buffer(std::move(other._local_buffer)) 
	{ other.invalidate(); }

	// copy assignment operator
	ObjectManager& operator=(const ObjectManager& other)
	noexcept(noexcept(ErrorPolicy::on_copy())
	         && OwnershipPolicy::can_nothrow_copy)
	{
		ObjectManager{other}.swap(*this);
		return *this;
	}

	// move assignment operator
	ObjectManager& operator=(ObjectManager&& other) noexcept
	{
		if constexpr (ErrorPolicy::check_before_destroy) {
			if (is_invalid()) { ErrorPolicy::on_destroy(); }
			else { OwnershipPolicy::destroy(m_allocator(), m_stored_object_ptr()); }
		}
		else { OwnershipPolicy::destroy(m_allocator(), m_stored_object_ptr()); }

		AllocatorType::operator=(std::move(other));
		_local_buffer = std::move(other._local_buffer);
		other.invalidate();

		return *this;
	}

	////////////////////////

	// make different template instantiations friends to allow conversions
	// unfortunately, this gives more access than necessary but avoids
	// complications and unreadibility that attorney-client idiom would cause
	template <typename, template <typename, template <typename> class> class,
	          class, std::size_t, template <typename> class>
	friend class ObjectManager;

	// since partial template specializations are not allowed to be friends,
	// one can at least restrict access to other template instantiations by
	// requiring them to be callable with this function, i.e. to be friendly
	template <template <typename, template <typename> class> class Ownsp, class Err>
	static constexpr bool
	is_friendly(const ObjectManager<TargetType, Ownsp, Err, BufferSize, Allocator>&);

	// general swap
	template <class Other>
	requires (requires(Other other) { is_friendly(other); })
	void swap(Other&& other) noexcept
	{
		// check if allocators need to be swapped
		if constexpr (OwnershipPolicy::AllocatorTraits::propagate_on_container_swap::value
			          || std::decay_t<Other>::AllocatorTraits::propagate_on_container_swap::value)
		{
			using std::swap;
			swap(m_allocator(), other.m_allocator());
		}

		Buffer tmp_buffer{nullptr};

		tmp_buffer = std::move(other._local_buffer);
		other._local_buffer = std::move(_local_buffer);
		_local_buffer = std::move(tmp_buffer);
	}

	// conversion constructor
	template <class Other, class OtherErr = std::decay_t<Other>>
	requires (requires(Other other) { is_friendly(other); }
		  && ManagerHelper::is_copy_constructible)
	explicit ObjectManager(Other&& other)
	noexcept (noexcept(OtherErr::on_copy())
	       && noexcept(ObjectManager{other.access()}))
		: AllocatorType(std::forward<Other>(other)),
		  OwnershipPolicy(std::forward<Other>(other)),
		  ErrorPolicy(std::forward<Other>(other))
	{
		if constexpr (OtherErr::check_before_copy) {
			if (other.is_invalid()) {
				OtherErr::on_copy();
				invalidate();
				return;
			}
		}

		OwnershipPolicy::create(m_allocator(), m_stored_object_ptr(),
		                        std::forward<Other>(other).access());
	}

	////////////////////////

	void invalidate() noexcept {
		// setting head to nullptr won't work because target object can have
		// first sizeof(void*) bytes set to zero but still be valid
		_local_buffer.head() = _local_buffer.address();
	}

	[[nodiscard]]
	bool is_invalid() const noexcept {
		return _local_buffer.head() == _local_buffer.address();
	}
};

} // namespace loblib::object_manager

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER
