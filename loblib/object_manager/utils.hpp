#ifndef LOBLIB_OBJECT_MANAGER_UTILS
#define LOBLIB_OBJECT_MANAGER_UTILS

#include <cstddef>
#include <type_traits>
#include <utility>
#include <algorithm>

///////////////////////////////////////////////////////

namespace loblib::object_manager
{

namespace internal
{

template <std::size_t Size>
union Any
{
	void* _head = nullptr;
	alignas(void*) std::byte _data[std::max<std::size_t>(Size, 1)];

	Any() noexcept {};
	Any(std::nullptr_t) noexcept {};
	Any& operator=(std::nullptr_t) noexcept {
		_head = nullptr;
		return *this;
	}

	[[nodiscard]]
	void* address() noexcept {
		return &_data[0];
	}

	[[nodiscard]]
	const void* address() const noexcept {
		return &_data[0];
	}

	[[nodiscard]]
	void*& head() noexcept {
		return _head;
	}

	[[nodiscard]]
	void* const& head() const noexcept {
		return _head;
	}

	template <typename T>
	requires std::is_const_v<std::remove_reference_t<T>>
	[[nodiscard]]
	T interpret_as() const noexcept {
		static_assert(sizeof(T) <= Size,
		              "'Any' is smaller than the requested object!");
		using Stored = std::decay_t<T>;
		return std::forward<T>(*static_cast<const Stored*>(address()));
	}

	template <typename T>
	[[nodiscard]]
	T interpret_as() noexcept {
		// implementation in terms of const version
		using CT = const std::remove_reference_t<T>&;
		return const_cast<T&&>((static_cast<const Any&>(*this)).interpret_as<CT>());
	}

	template <typename T>
	static consteval bool can_construct() {
		return (sizeof(T) <= Size) && (alignof(T) <= alignof(void*))
		       && (alignof(void*) % alignof(T) == 0);
	}
};

template <typename Cleanup>
class ScopeGuard
{
  private:
	Cleanup _cleanup;
	bool _guard;

  public:
	ScopeGuard(Cleanup&& cleanup)
	requires(std::is_invocable_v<Cleanup>)
		: _cleanup(std::forward<Cleanup>(cleanup)), _guard(true) {}
	~ScopeGuard() {
		if (_guard) { _cleanup(); }
	}

	void disarm() noexcept { _guard = false; }
};

// remove rvalue references and keep lvalue references
template <typename T>
using strip_rvalue_t =
	std::conditional_t<std::is_rvalue_reference_v<T>, std::remove_reference_t<T>, T>;

// deduction guide:
// constructor argument is an lvalue -> store a reference only
// otherwise -> copy the callable object
template <typename T>
ScopeGuard(T&&) -> ScopeGuard<strip_rvalue_t<T>>;

} // namespace internal
} // namespace loblib::object_manager

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER_UTILS
