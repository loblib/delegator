#ifndef LOBLIB_OBJECT_MANAGER_ERROR_POLICIES
#define LOBLIB_OBJECT_MANAGER_ERROR_POLICIES

#include <cassert>
#include <stdexcept>

///////////////////////////////////////////////////////

namespace loblib::object_manager
{

namespace error_policies
{

class CheckNone;
class SkipOnError;
class AssertOnError;
class ThrowOnError;

class CheckNone
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_copy = false;
	static constexpr bool check_before_destroy = false;

	static void on_access() noexcept {}
	static void on_copy() noexcept {}
	static void on_destroy() noexcept {}

	CheckNone() = default;
	~CheckNone() = default;

	explicit CheckNone(const SkipOnError&) {}
	explicit CheckNone(const AssertOnError&) {}
	explicit CheckNone(const ThrowOnError&) {}
};

class SkipOnError
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_copy = true;
	static constexpr bool check_before_destroy = true;

	static void on_access() noexcept {}
	static void on_copy() noexcept {}
	static void on_destroy() noexcept {}

	SkipOnError() = default;
	~SkipOnError() = default;

	SkipOnError(const CheckNone&) {}
	explicit SkipOnError(const AssertOnError&) {}
	explicit SkipOnError(const ThrowOnError&) {}
};

class AssertOnError : protected SkipOnError
{
  protected:
	static void on_access() noexcept { assert(false); }
	static void on_copy() noexcept { assert(false); }
	// 'on_destroy' is empty because assertion on destruction could possibly affect performance

	AssertOnError() = default;
	~AssertOnError() = default;

	AssertOnError(const CheckNone&) {}
	AssertOnError(const SkipOnError&) {}
	explicit AssertOnError(const ThrowOnError&) {}
};

class ThrowOnError : protected SkipOnError
{
  protected:
	static void on_access() {
		throw std::logic_error("Attempted to access an invalid object!");
	}
	static void on_copy() {
		throw std::logic_error("Attempted to copy an invalid object!");
	}
	// 'on_destroy' is empty because destruction should not throw!

	ThrowOnError() = default;
	~ThrowOnError() = default;

	ThrowOnError(const CheckNone&) {}
	ThrowOnError(const SkipOnError&) {}
	ThrowOnError(const AssertOnError&) {}
};

} // namespace error_policies
} // namespace loblib::object_manager

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER_ERROR_POLICIES
