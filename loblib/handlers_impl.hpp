
///////////////////////////////////////////////////////

// NOTE:
// this section will never be actually included
// written only for debugging purposes

#ifndef LOBLIB_DELEGATE_HANDLERS

#	include <cstddef>
#	include <type_traits>
#	include <utility>
#	include <functional>

#	include "utils.hpp"
#	include "object_manager/object_manager.hpp"

using namespace loblib;
using namespace loblib::delegate;
using namespace loblib::delegate::internal;

template <typename Signature>
class SimpleHandlerVTable;

template <typename Signature>
class DestructibleHandlerVTable;

template <typename Signature>
class CloneableHandlerVTable;

template 
<
	typename Signature, 
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator
>
class VTableHandler;

#endif // !LOBLIB_DELEGATE_HANDLERS

///////////////////////////////////////////////////////

#ifndef _LOBLIBCXX_FTOR_CV
#	define _LOBLIBCXX_FTOR_CV
#endif

#ifdef _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF
#else
#	define _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV&
#endif

#define _LOBLIBCXX_FTOR_CV_REF _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class SimpleHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

  protected:
	~SimpleHandlerVTable() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class DestructibleHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

	virtual ~DestructibleHandlerVTable() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class CloneableHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

	virtual void clone_itself(void* destination) const = 0;
	virtual ~CloneableHandlerVTable() = default;
};

template 
<
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator,
	typename ReturnType,
	bool Noexcept,
	typename... ParamTypes
>
class VTableHandler <
			ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept), 
			InvocableType, 
			VTable, 
			OwnershipPolicy, 
			BufferSize, 
			AcceptPointers, 
			Allocator
		>
	final : public VTable
{
  private:
	using Manager = object_manager::ObjectManager <
						InvocableType, 
						OwnershipPolicy, 
						object_manager::CheckNone,
						BufferSize, 
						Allocator
					>;
	Manager _manager;

	template <typename T>
	static consteval bool m_basic_constraint() {
		return std::is_constructible_v<std::decay_t<InvocableType>, T>
		       && (!std::is_same_v<VTableHandler, std::remove_cvref_t<T>>);
	}

	template <typename... Args>
	requires (sizeof...(Args) > 1)
	static consteval bool m_basic_constraint()
	{ return std::is_constructible_v<std::decay_t<InvocableType>, Args...>; }

  public:
	static constexpr bool using_soo = Manager::using_soo;

	template <typename... Args>
	requires (m_basic_constraint<Args...>())
	VTableHandler(Args&&... args)
	noexcept( std::is_nothrow_constructible_v<Manager, Args...> )
		: _manager(std::forward<Args>(args)...) {}

	VTableHandler(const VTableHandler& other) = default;
	VTableHandler(VTableHandler&& other) noexcept = delete;

	VTableHandler& operator=(const VTableHandler&) = delete;
	VTableHandler& operator=(VTableHandler&&) noexcept = delete;

	~VTableHandler() = default;

	// this one is not marked 'final' because 'clone_itself' might not be in the vtable
	void clone_itself(void* destination) const {
		::new (destination) VTableHandler(*this);
	}

	ReturnType operator()(RefNonTrivials<ParamTypes>... params)
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) final {

		using FunInvQuals = InvocableType _LOBLIBCXX_FTOR_INV_QUALS;

		if constexpr (
			is_invocable<FunInvQuals, ReturnType, Noexcept, ParamTypes...>::value) {
			return std::invoke(
				std::forward<FunInvQuals>(_manager.access()),
				std::forward<RefNonTrivials<ParamTypes>>(
					params)...);
		}
		else if constexpr (AcceptPointers && is_dereferencable_v<InvocableType>) {
			using DereferencedInvQuals =
				Dereferenced<InvocableType> _LOBLIBCXX_FTOR_INV_QUALS;

			if constexpr (is_invocable<DereferencedInvQuals, ReturnType,
			                           Noexcept, ParamTypes...>::value) {
				return std::invoke(
					std::forward<DereferencedInvQuals>(*_manager.access()),
					std::forward<RefNonTrivials<ParamTypes>>(
						params)...);
			}
			else {
				static_assert(make_false<InvocableType>(),
				              "Target does not point to an invocable object!");
			}
		}
		else {
			static_assert(make_false<InvocableType>(),
			              "Target is not invocable!");
		}
	}
};

#undef _LOBLIBCXX_FTOR_CV_REF
#undef _LOBLIBCXX_FTOR_CV
#undef _LOBLIBCXX_FTOR_REF
#undef _LOBLIBCXX_FTOR_INV_QUALS

///////////////////////////////////////////////////////

