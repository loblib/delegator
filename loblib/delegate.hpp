#ifndef LOBLIB_DELEGATE
#define LOBLIB_DELEGATE

#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>
#include <initializer_list>
#include <functional>

#include "error_policies.hpp"
#include "storage_policies.hpp"
#include "acceptance_policies.hpp"
#include "object_manager/ownership_policies.hpp"

///////////////////////////////////////////////////////

namespace loblib::delegate
{

using namespace error_policies;
using namespace storage_policies;
using namespace acceptance_policies;
using namespace object_manager::ownership_policies;

template 
<
	typename Signature,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy = OwningStorage,
	template <typename, template <typename> class> 
	class OwnershipPolicy = ReferenceCounting,
	template <typename> 
	class AcceptancePolicy = StandardAccept,
	class ErrorPolicy = AssertOnError, 
	std::size_t BufferSize = sizeof(void*),
	template <typename> 
	class Allocator = std::allocator
>
class Delegate;

///////////////////////////////////////////////////////

template <typename Signature>
using MoveOnlyDelegate =
	Delegate<Signature, OwningStorage, DeepCopy, NonCopyableAccept>;

template <typename Signature>
using DelegateRef =
	Delegate<Signature, RefOnlyStorage, LocalOwnership, StandardAccept>;

template <typename Signature>
using StandardDelegate =
	Delegate<Signature, OwningStorage, DeepCopy,
              StandardAccept, ThrowOnError, internal::MPtrSize>;

template <typename Signature, std::size_t BufferSize>
using SzDelegate = Delegate<Signature, OwningStorage, ReferenceCounting,
                              StandardAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzMoveOnlyDelegate =
	Delegate<Signature, OwningStorage, DeepCopy, NonCopyableAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzDelegateRef = Delegate<Signature, RefOnlyStorage, LocalOwnership,
                                 StandardAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzStandardDelegate =
	Delegate<Signature, OwningStorage, DeepCopy, StandardAccept, ThrowOnError, BufferSize>;

///////////////////////////////////////////////////////

template <typename>
inline constexpr bool is_delegate_v = false;

template 
<
	typename Sgn,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class Storg,
	template <typename, template <typename> class> 
	class Ownsp,
	template <typename> 
	class Acc,
	class Err, 
	std::size_t S,
	template <typename> 
	class Alloc
>
inline constexpr bool is_delegate_v<Delegate<Sgn, Storg, Ownsp, Acc, Err, S, Alloc>> =
	true;

template <typename T>
using is_delegate = std::bool_constant<is_delegate_v<T>>;

///////////////////////////////////////////////////////

namespace internal
{

template <class Storage>
class DelegateBase : public Storage
{
  protected:
	using StorageType = Storage;
	Storage::Buffer _local_buffer{nullptr};

	DelegateBase() noexcept = default;

	template <typename Fun, typename... Args>
	static consteval bool check_nothrow() noexcept {
		return Storage::template Traits<Fun>::template is_nothrow_constructible<Args...>;
	}

	// general ctor
	template <typename Fun, typename... Args>
	DelegateBase(std::in_place_type_t<Fun>, Args&&... args)
	noexcept(check_nothrow<std::decay_t<Fun>, Args...>())
	{
		// if there is only one argument and is null, do not spend time on construction
		if constexpr (sizeof...(Args) == 1) {
			if constexpr (internal::is_function_pointer_v<std::remove_cvref_t<Fun>>
			              || std::is_member_pointer_v<std::decay_t<Fun>>
			              || is_delegate_v<std::decay_t<Fun>>) {

				// this is actually only testing if the first (and the only one) argument is null
				if ((args == ... == nullptr)) { return; }
			}
		}

		Storage::template create<Fun>(_local_buffer, std::forward<Args>(args)...);
	}

	// dtor
	~DelegateBase() { Storage::destroy(_local_buffer); }

	// copy ctor
	DelegateBase(const DelegateBase& other) {
		Storage::copy(other._local_buffer, _local_buffer);
	}

	// move ctor
	DelegateBase(DelegateBase&& other) noexcept
		: _local_buffer(std::move(other._local_buffer)) {
		Storage::set_to_empty(other._local_buffer);
	}

	// construct as empty
	DelegateBase(std::nullptr_t) noexcept {
		Storage::set_to_empty(_local_buffer);
	}

	// copy assignment operator
	DelegateBase& operator=(const DelegateBase& other) {
		DelegateBase{other}.swap(*this);
		return *this;
	}

	// move assignment operator
	DelegateBase& operator=(DelegateBase&& other) noexcept {
		Storage::destroy(_local_buffer);
		_local_buffer = std::move(other._local_buffer);
		Storage::set_to_empty(other._local_buffer);

		return *this;
	}

	// assignment to empty
	DelegateBase& operator=(std::nullptr_t) noexcept {
		Storage::destroy(_local_buffer);
		Storage::set_to_empty(_local_buffer);

		return *this;
	}

	void swap(DelegateBase& other) noexcept {
		typename Storage::Buffer tmp_buffer{nullptr};

		tmp_buffer = std::move(other._local_buffer);
		other._local_buffer = std::move(_local_buffer);
		_local_buffer = std::move(tmp_buffer);
	}
};

} // namespace internal

///////////////////////////////////////////////////////

#include "delegate_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "delegate_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "delegate_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "delegate_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "delegate_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "delegate_impl.hpp"

} // namespace loblib::delegate

///////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE
