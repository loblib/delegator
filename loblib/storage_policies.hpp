#ifndef LOBLIB_DELEGATE_STORAGE_POLICIES
#define LOBLIB_DELEGATE_STORAGE_POLICIES

#include <cstddef>
#include <type_traits>
#include <algorithm>
#include <exception>

#include "handlers.hpp"
#include "object_manager/object_manager.hpp"

///////////////////////////////////////////////////////

namespace loblib::delegate
{

namespace internal
{

using object_manager::internal::Any;
using object_manager::internal::LocalOwnership;
using object_manager::ownership_policies::is_same_ownership_policy_v;

template <typename Buffer, typename Handler>
struct StorageTraits
{
  public:
	template <typename... Args>
	static constexpr bool is_constructible =
		Buffer::template can_construct<Handler>()
		&& std::is_constructible_v<Handler, Args...>;

	template <typename... Args>
	static constexpr bool is_nothrow_constructible =
		is_constructible<Args...>
		&& std::is_nothrow_constructible_v<Handler, Args...>;
};

///////////////////////////////////////////////////////

template 
<
	typename Signature, 
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize
>
class ManagingStorageBase 
	: public AcceptancePolicy<Signature>
	, public ErrorPolicy
{
  protected:
	using ErrorHandler = ErrorPolicy;
	using Acceptance = AcceptancePolicy<Signature>;
	using HandlerVTable =
		std::conditional_t<Acceptance::AcceptNonCopyables,
	                       internal::DestructibleHandlerVTable<Signature>,
	                       internal::CloneableHandlerVTable<Signature>>;

	static constexpr std::size_t Size =
		internal::AddPaddingToSize<alignof(void*)>(sizeof(HandlerVTable))
		+ std::max<int>(BufferSize, sizeof(void*));

	static constexpr std::size_t Alignment =
		std::max<int>(alignof(HandlerVTable), alignof(void*));

	static constexpr std::size_t PaddedSize =
		internal::AddPaddingToSize<Alignment>(Size);

	using Buffer = Any<PaddedSize>;

	////////////////////////

	static consteval bool can_nothrow_access() {
		if constexpr (ErrorHandler::check_before_access) {
			return noexcept(ErrorHandler::on_access());
		}
		return true;
	}

	[[nodiscard]]
	static const HandlerVTable& access(const Buffer& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		if constexpr (ErrorHandler::check_before_access) {
			if (is_empty(_local_buffer)) {
				ErrorHandler::on_access();
				std::terminate();
			}
		}

		return _local_buffer.template interpret_as<const HandlerVTable&>();
	}

	[[nodiscard]]
	static HandlerVTable& access(Buffer& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return const_cast<HandlerVTable&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	[[nodiscard]]
	static const HandlerVTable&& access(const Buffer&& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return static_cast<const HandlerVTable&&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	[[nodiscard]]
	static HandlerVTable&& access(Buffer&& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return const_cast<HandlerVTable&&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	////////////////////////

	static void copy(const Buffer& source, Buffer& destination)
	requires(!Acceptance::AcceptNonCopyables)
	{
		if constexpr (ErrorHandler::check_before_copy) {
			if (is_empty(source)) {
				ErrorHandler::on_copy();
				set_to_empty(destination);
				return;
			}
		}

		(access(source)).clone_itself(destination.address());
	}

	static void destroy(const Buffer& target) noexcept {
		if constexpr (ErrorHandler::check_before_destroy) {
			if (is_empty(target)) {
				ErrorHandler::on_destroy();
				return;
			}
		}

		access(target).~HandlerVTable();
	}

	////////////////////////

	[[nodiscard]]
	static bool is_empty(const Buffer& _local_buffer) noexcept {
		return _local_buffer.head() == nullptr;
	}

	static void set_to_empty(Buffer& _local_buffer) noexcept {
		_local_buffer = nullptr;
	}

	////////////////////////

	ManagingStorageBase() = default;
	~ManagingStorageBase() = default;
};

///////////////////////////////////////////////////////

template 
<
	typename Signature, 
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize
>
class NonManagingStorageBase 
	: public AcceptancePolicy<Signature>
	, public ErrorPolicy
{
  protected:
	using ErrorHandler = ErrorPolicy;
	using Acceptance = AcceptancePolicy<Signature>;
	using HandlerVTable = internal::SimpleHandlerVTable<Signature>;

	static constexpr std::size_t Size = sizeof(HandlerVTable) + BufferSize;
	static constexpr std::size_t Alignment = alignof(HandlerVTable);

	static constexpr std::size_t StorageSize =
		internal::AddPaddingToSize<Alignment>(Size);

	using Buffer = Any<StorageSize>;

	////////////////////////

	static consteval bool can_nothrow_access() {
		if constexpr (ErrorHandler::check_before_access) {
			return noexcept(ErrorHandler::on_access());
		}
		return true;
	}

	[[nodiscard]]
	static const HandlerVTable& access(const Buffer& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		if constexpr (ErrorHandler::check_before_access) {
			if (is_empty(_local_buffer)) {
				ErrorHandler::on_access();
				std::terminate();
			}
		}

		return _local_buffer.template interpret_as<const HandlerVTable&>();
	}

	[[nodiscard]]
	static HandlerVTable& access(Buffer& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return const_cast<HandlerVTable&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	[[nodiscard]]
	static const HandlerVTable&& access(const Buffer&& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return static_cast<const HandlerVTable&&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	[[nodiscard]]
	static HandlerVTable&& access(Buffer&& _local_buffer) 
	noexcept(can_nothrow_access()) 
	{
		return const_cast<HandlerVTable&&>(
			access(static_cast<const Buffer&>(_local_buffer)));
	}

	////////////////////////

	static void copy(const Buffer& source, Buffer& destination)
	noexcept(noexcept(ErrorHandler::on_copy()))
	requires(!Acceptance::AcceptNonCopyables)
	{
		if constexpr (ErrorHandler::check_before_copy) {
			if (is_empty(source)) {
				ErrorHandler::on_copy();
				set_to_empty(destination);
				return;
			}
		}

		destination = source;
	}

	static void destroy(const Buffer& target) noexcept {
		if constexpr (ErrorHandler::check_before_destroy) {
			if (is_empty(target)) { ErrorHandler::on_destroy(); }
		}

		// Non-managing storage assumes destructor does not need to be called!!!
	}

	////////////////////////

	[[nodiscard]]
	static bool is_empty(const Buffer& _local_buffer) noexcept {
		return _local_buffer.head() == nullptr;
	}

	static void set_to_empty(Buffer& _local_buffer) noexcept {
		_local_buffer = nullptr;
	}

	////////////////////////

	NonManagingStorageBase() = default;
	~NonManagingStorageBase() = default;
};

} // namespace internal

///////////////////////////////////////////////////////

namespace storage_policies
{

template 
<
	typename Signature, 
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
class OwningStorage
	: protected internal::ManagingStorageBase <
					Signature, 
					AcceptancePolicy, 
					ErrorPolicy, 
					BufferSize
				>
{
  private:
	using Parent = internal::ManagingStorageBase <
						Signature, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize
					>;

	using typename Parent::Acceptance;
	using typename Parent::HandlerVTable;

	template <typename Fun>
	using Handler = internal::VTableHandler <
						Signature, 
						std::decay_t<Fun>, 
						HandlerVTable, 
						OwnershipPolicy, 
						BufferSize,
						Acceptance::AcceptPointers, 
						Allocator
					>;

  protected:
	using typename Parent::ErrorHandler;
	using typename Parent::Buffer;

	template <typename Fun>
	struct Traits : public internal::StorageTraits<Buffer, Handler<Fun>>
	{
		static constexpr bool using_soo = Handler<Fun>::using_soo;
		static constexpr bool storing_reference = false;
	};

	template <typename Fun, typename... Args>
	requires(Acceptance::template is_eligible<Fun>)
	static void create(Buffer& target, Args&&... args)
	noexcept(std::is_nothrow_constructible_v<Handler<Fun>, Args...>)
	{
		::new (target.address()) Handler<Fun>(std::forward<Args>(args)...);

		static_assert(Traits<Fun>::template is_constructible<Args...>,
		              "Storage is not constructible!");
	}

	OwningStorage() = default;
	~OwningStorage() = default;
};

///////////////////////////////////////////////////////

template 
<
	typename Signature, 
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
class NonOwningStorage
	: protected internal::ManagingStorageBase <
					Signature, 
					AcceptancePolicy, 
					ErrorPolicy, 
					BufferSize
				>
{
  private:
	using Parent = internal::ManagingStorageBase <
						Signature, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize
					>;

	using typename Parent::Acceptance;
	using typename Parent::HandlerVTable;

	template <typename Fun>
	static consteval bool m_can_use_soo() {
		using Handler = internal::VTableHandler <
							Signature, 
							std::decay_t<Fun>, 
							HandlerVTable, 
							OwnershipPolicy, 
							BufferSize,
							Acceptance::AcceptPointers, 
							Allocator
						>;

		return Handler::using_soo;
	}

	template <typename Fun>
	struct Handler
	{
		static constexpr bool _storing_reference =
			std::is_lvalue_reference_v<Fun> && !m_can_use_soo<Fun>();

		static constexpr bool _accept_ptrs = _storing_reference
		                                     || Acceptance::AcceptPointers;

		using target = std::conditional_t <
							_storing_reference, 
							std::remove_reference_t<Fun>*, 
							std::decay_t<Fun>
						>;
		using type = internal::VTableHandler <
							Signature, 
							target, 
							HandlerVTable, 
							OwnershipPolicy, 
							BufferSize,
							_accept_ptrs, 
							Allocator
						>;
	};

  protected:
	using typename Parent::ErrorHandler;
	using typename Parent::Buffer;

	template <typename Fun>
	struct Traits
		: public internal::StorageTraits<Buffer, typename Handler<Fun>::type>
	{
		static constexpr bool using_soo = m_can_use_soo<Fun>();
		static constexpr bool storing_reference = Handler<Fun>::_storing_reference;
	};

	template <typename Fun>
	requires(Traits<Fun>::storing_reference && Acceptance::template is_eligible<Fun>)
	static void create(Buffer& target, Fun invocable) noexcept {
		::new (target.address()) Handler<Fun>::type(std::addressof(invocable));

		static_assert(
			Traits<Fun>::template is_constructible<typename Handler<Fun>::target>,
			"Storage is not constructible!");
	}

	template <typename Fun, typename... Args>
	requires(Acceptance::template is_eligible<Fun>)
	static void create(Buffer& target, Args&&... args)
	noexcept(std::is_nothrow_constructible_v<typename Handler<Fun>::type, Args...>)
	{
		::new (target.address()) Handler<Fun>::type(std::forward<Args>(args)...);

		static_assert(Traits<Fun>::template is_constructible<Args...>,
		              "Storage is not constructible!");
	}

	NonOwningStorage() = default;
	~NonOwningStorage() = default;
};

///////////////////////////////////////////////////////

using internal::LocalOwnership;

///////////////////////////////////////////////////////

template 
<
	typename Signature, 
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
requires internal::is_same_ownership_policy_v<OwnershipPolicy, LocalOwnership>
class NonManagingStorage
	: protected internal::NonManagingStorageBase <
					Signature, 
					AcceptancePolicy, 
					ErrorPolicy, 
					BufferSize
				>
{
  private:
	using Parent = internal::NonManagingStorageBase <
						Signature, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize
					>;

	using typename Parent::Acceptance;
	using typename Parent::HandlerVTable;

	template <typename Fun>
	using Handler = internal::VTableHandler <
						Signature, 
						std::decay_t<Fun>, 
						HandlerVTable, 
						LocalOwnership, 
						BufferSize,
						Acceptance::AcceptPointers, 
						Allocator
					>;

  protected:
	using typename Parent::ErrorHandler;
	using typename Parent::Buffer;

	template <typename Fun>
	struct Traits : public internal::StorageTraits<Buffer, Handler<Fun>>
	{
		static constexpr bool using_soo = true;
		static constexpr bool storing_reference = false;
	};

	template <typename Fun, typename... Args>
	requires(Acceptance::template is_eligible<Fun>)
	static void create(Buffer& target, Args&&... args)
	noexcept(std::is_nothrow_constructible_v<Handler<Fun>, Args...>)
	{

		static_assert(Handler<Fun>::using_soo,
		              "Non-managing storage cannot be used with objects that "
		              "are not locally constructible!");
		static_assert(std::is_trivially_constructible_v<std::decay_t<Fun>, Args...>,
		              "Non-managing storage cannot be used with objects that "
		              "are not trivially constructible!");

		::new (target.address()) Handler<Fun>(std::forward<Args>(args)...);

		static_assert(Traits<Fun>::template is_constructible<Args...>,
		              "Storage is not constructible!");
	}
};

///////////////////////////////////////////////////////

template 
<
	typename Signature, 
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
requires internal::is_same_ownership_policy_v<OwnershipPolicy, LocalOwnership>
class RefOnlyStorage
	: protected internal::NonManagingStorageBase <
					Signature, 
					AcceptancePolicy, 
					ErrorPolicy, 
					BufferSize
				>
{
  private:
	using Parent = internal::NonManagingStorageBase <
						Signature, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize
					>;

	using typename Parent::Acceptance;
	using typename Parent::HandlerVTable;

	template <typename Fun>
	using Handler = internal::VTableHandler <
						Signature, 
						std::remove_reference_t<Fun>*, 
						HandlerVTable, 
						LocalOwnership, 
						BufferSize,
						true, 
						Allocator
					>;

  protected:
	using typename Parent::ErrorHandler;
	using typename Parent::Buffer;

	template <typename Fun>
	struct Traits : public internal::StorageTraits<Buffer, Handler<Fun>>
	{
		static constexpr bool using_soo = true;
		static constexpr bool storing_reference = true;
	};

	template <typename Fun>
	requires(Acceptance::template is_eligible<Fun>)
	static void create(Buffer& target, Fun&& invocable) noexcept
	requires(Traits<Fun>::template is_constructible<std::remove_reference_t<Fun>*>)
	{
		::new (target.address()) Handler<Fun>(std::addressof(invocable));

		static_assert(
			Traits<Fun>::template is_constructible<std::remove_reference_t<Fun>*>,
			"Storage is not constructible!");
	}
};

///////////////////////////////////////////////////////

template 
<
	template 
	<
		typename, 
		template <typename, template <typename> class> class,
		template <typename> class, 
		class, 
		std::size_t,
		template <typename> class
	>
	class,

	template 
	<
		typename, 
		template <typename, template <typename> class> class,
		template <typename> class, 
		class, 
		std::size_t,
		template <typename> class
	>
	class
>
class is_same_storage_policy : public std::false_type
{};

template 
<
	template 
	<
		typename, 
		template <typename, template <typename> class> class,
		template <typename> class, 
		class, 
		std::size_t,
		template <typename> class
	>
	class SP
>
class is_same_storage_policy<SP, SP> : public std::true_type
{};

template 
<
	template 
	<
		typename, 
		template <typename, template <typename> class> class,
		template <typename> class, 
		class, 
		std::size_t,
		template <typename> class
	>
	class SP1,

	template 
	<
		typename, 
		template <typename, template <typename> class> class,
		template <typename> class, 
		class, 
		std::size_t,
		template <typename> class
	>
	class SP2
>
inline constexpr bool is_same_storage_policy_v =
	is_same_storage_policy<SP1, SP2>::value;

} // namespace storage_policies
} // namespace loblib::delegate

/////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE_STORAGE_POLICIES
