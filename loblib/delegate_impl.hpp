
///////////////////////////////////////////////////////

// NOTE:
// this section will never be actually included
// written only for debugging purposes

#ifndef LOBLIB_DELEGATE

#	include <cstddef>
#	include <type_traits>
#	include <utility>
#	include <memory>
#	include <initializer_list>
#	include <functional>

#	include "error_policies.hpp"
#	include "storage_policies.hpp"
#	include "acceptance_policies.hpp"
#	include "object_manager/ownership_policies.hpp"

using namespace loblib;
using namespace loblib::delegate;

namespace loblib::delegate::internal
{

template <class Storage>
class DelegateBase;

}

template 
<
	typename Signature,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy,
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy,
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
class Delegate;

template <typename>
inline constexpr bool is_delegate_v = false;

#endif // !LOBLIB_DELEGATE

///////////////////////////////////////////////////////

#ifndef _LOBLIBCXX_FTOR_CV
#	define _LOBLIBCXX_FTOR_CV
#endif

#ifdef _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF
#else
#	define _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV&
#endif

#define _LOBLIBCXX_FTOR_CV_REF _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF

template 
<
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy,
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy,
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator,
	typename ReturnType,
	bool Noexcept,
	typename... ParamTypes
>
class Delegate
	<
		ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept), 
		StoragePolicy, 
		OwnershipPolicy, 
		AcceptancePolicy, 
		ErrorPolicy, 
		BufferSize, 
		Allocator
	>
	: private internal::DelegateBase <
			StoragePolicy
			<
				ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept),
				OwnershipPolicy, 
				AcceptancePolicy, 
				ErrorPolicy, 
				BufferSize, 
				Allocator
			> >
{
  private:
	using Signature = ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept);
	using Base = internal::DelegateBase <
					StoragePolicy
					<
						Signature, 
						OwnershipPolicy, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize, 
						Allocator
					> >;

	using Storage = Base::StorageType;
	using Base::_local_buffer;

  public:
	Delegate() = delete;
	~Delegate() = default;
	Delegate(const Delegate& other) = default;
	Delegate(Delegate&& other) noexcept = default;
	Delegate& operator=(const Delegate&) = default;
	Delegate& operator=(Delegate&&) noexcept = default;

	Delegate(std::nullptr_t) noexcept : Base(nullptr) {}
	Delegate& operator=(std::nullptr_t) noexcept {
		Base::operator=(nullptr);
		return *this;
	}

	// allow users to check if their target object would be constructed locally
	template <typename Fun>
	static constexpr bool using_soo = Storage::template Traits<Fun>::using_soo;

	/* NOTE: Creation and destruction should be delegated to the base class since the base
	class manages the storage! Otherwise, undefined behaviour can easily arise. For example,
	if an exception is thrown during construction and construction is not done in the base
	class constructor, destructor will be called on unconstructed object! */

	// general constructor
	template <typename Fun, typename DFun = std::decay_t<Fun>>
	requires (!std::is_same_v<DFun, Delegate>) && (!internal::is_in_place_type_v<DFun>)
	Delegate(Fun&& fun)
	noexcept(Base::template check_nothrow<DFun, Fun>())
	: Base(std::in_place_type<Fun>, std::forward<Fun>(fun))
	{
		// an extra invocability check for a more direct error message
		static_assert(Storage::template is_invocable_v<DFun>);
	}

	// to allow in-place construction
	template <typename Fun, typename... Args>
	requires std::is_constructible_v<Fun, Args...>
	explicit Delegate(std::in_place_type_t<Fun>, Args&&... args)
	noexcept(Base::template check_nothrow<Fun, Args...>())
	: Base(std::in_place_type<Fun>, std::forward<Args>(args)...)
	{
		static_assert(std::is_same_v<std::decay_t<Fun>, Fun>);
		static_assert(Storage::template is_invocable_v<std::decay_t<Fun>>);
	}

	// to allow in-place construction with an initializer list
	template <typename Fun, typename T, typename... Args>
	requires std::is_constructible_v<Fun, std::initializer_list<T>&, Args...>
	explicit Delegate
		(
			std::in_place_type_t<Fun>,
			std::initializer_list<T> init_list,
			Args&&... args
		) 
	noexcept(Base::template check_nothrow<Fun, std::initializer_list<T>&, Args...>())
	: Base(std::in_place_type<Fun>, init_list, std::forward<Args>(args)...)
	{
		static_assert(std::is_same_v<std::decay_t<Fun>, Fun>);
		static_assert(Storage::template is_invocable_v<std::decay_t<Fun>>);
	}

	// to allow changing the target object
	template <typename Fun, typename DFun = std::decay_t<Fun>>
	requires std::is_constructible_v<Delegate, Fun> && (!std::is_same_v<DFun, Delegate>)
	Delegate& operator=(Fun&& fun)
	noexcept(Base::template check_nothrow<DFun, Fun>())
	{
		Delegate{std::forward<Fun>(fun)}.swap(*this);
		return *this;
	}

	ReturnType operator()(ParamTypes... params) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) {

		if constexpr (Storage::ErrorHandler::check_before_call) {
			if (Storage::is_empty(_local_buffer)) {
				Storage::ErrorHandler::on_invoke();
			}
		}

		using BufferInvQuals = Storage::Buffer _LOBLIBCXX_FTOR_INV_QUALS;
		return std::invoke(Storage::access(std::forward<BufferInvQuals>(_local_buffer)),
		                   std::forward<ParamTypes>(params)...);
	}

	void swap(Delegate& other) noexcept { Base::swap(other); }

	friend void swap(Delegate& x, Delegate& y) noexcept { x.swap(y); }

	friend bool operator==(const Delegate& x, std::nullptr_t) noexcept {
		return Storage::is_empty(x._local_buffer);
	}

	/* NOTE: conversions between different template instantiations of `Delegate` are
	not implemented because, since the underlying type is erased, two template
	instantiations would have to have the same `clone_itself` function saved in the
	vtable, and requiring all of them to have the same vtable (both in type and content)
	would impose additional overhead and would restrict configurability. */
};

#undef _LOBLIBCXX_FTOR_CV_REF
#undef _LOBLIBCXX_FTOR_CV
#undef _LOBLIBCXX_FTOR_REF
#undef _LOBLIBCXX_FTOR_INV_QUALS

///////////////////////////////////////////////////////

