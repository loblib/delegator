#ifndef LOBLIB_DELEGATE_ACCEPTANCE_POLICIES
#define LOBLIB_DELEGATE_ACCEPTANCE_POLICIES

#include <type_traits>
#include "utils.hpp"

///////////////////////////////////////////////////////

namespace loblib::delegate
{

namespace acceptance_policies
{

template <typename Signature>
class StandardAccept;

template <typename Signature>
class NonCopyableAccept;

template <typename Signature>
class PtrAccept;

template <typename Signature>
class NonCopyablePtrAccept;

///////////////////////////////////////////////////////

#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "acceptance_policies_impl.hpp"

} // namespace acceptance_policies
} // namespace loblib::delegate

///////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE_ACCEPTANCE_POLICIES
