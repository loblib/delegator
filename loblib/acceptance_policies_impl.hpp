
///////////////////////////////////////////////////////

// NOTE:
// this section will never be actually included
// written only for debugging purposes

#ifndef LOBLIB_DELEGATE_ACCEPTANCE_POLICIES

#	include <type_traits>
#	include "utils.hpp"

using namespace loblib;
using namespace loblib::delegate;

template <typename Signature>
class StandardAccept;

template <typename Signature>
class NonCopyableAccept;

template <typename Signature>
class PtrAccept;

template <typename Signature>
class NonCopyablePtrAccept;

#endif // !LOBLIB_DELEGATE_ACCEPTANCE_POLICIES

///////////////////////////////////////////////////////

#ifndef _LOBLIBCXX_FTOR_CV
#	define _LOBLIBCXX_FTOR_CV
#endif

#ifdef _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF
#else
#	define _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV&
#endif

#define _LOBLIBCXX_FTOR_CV_REF _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class StandardAccept
	<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  protected:
	static constexpr bool AcceptPointers = false;
	static constexpr bool AcceptNonCopyables = false;

	template <typename T>
	static constexpr bool is_invocable_v =
		internal::is_invocable <
						T _LOBLIBCXX_FTOR_INV_QUALS, 
						ReturnType, 
						Noexcept, 
						ParamTypes...
					>::value
		&& internal::is_invocable <
						T _LOBLIBCXX_FTOR_REF, 
						ReturnType, 
						Noexcept, 
						ParamTypes...
					>::value;

	template <typename T, typename DT = std::decay_t<T>>
	static constexpr bool is_eligible =
		std::is_copy_constructible_v<DT> && is_invocable_v<DT>;

	StandardAccept() = default;
	~StandardAccept() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class NonCopyableAccept
	<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
	: protected StandardAccept<ReturnType(ParamTypes...)
                                   _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  private:
	using Parent = StandardAccept
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>;

  protected:
	static constexpr bool AcceptNonCopyables = true;

	template <typename T, typename DT = std::decay_t<T>>
	static constexpr bool is_eligible = Parent::template is_invocable_v<DT>;

	NonCopyableAccept() = default;
	~NonCopyableAccept() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class PtrAccept
	<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
	: protected StandardAccept
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  private:
	using Parent = StandardAccept
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>;

  protected:
	static constexpr bool AcceptPointers = true;

	template <typename T>
	static constexpr bool is_pointer_to_invocable_f() {
		if constexpr (internal::is_dereferencable_v<T>) {
			return Parent::template is_invocable_v<internal::Dereferenced<T>>;
		}
		else { return false; }
	}

	template <typename T>
	static constexpr bool is_pointer_to_invocable_v =
		is_pointer_to_invocable_f<T>();

	template <typename T, typename DT = std::decay_t<T>>
	static constexpr bool is_eligible =
		std::is_copy_constructible_v<DT>
		&& (Parent::template is_invocable_v<DT> || is_pointer_to_invocable_v<DT>);

	PtrAccept() = default;
	~PtrAccept() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class NonCopyablePtrAccept
	<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
	: protected PtrAccept
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  private:
	using Parent = PtrAccept
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>;

  protected:
	static constexpr bool AcceptNonCopyables = true;

	template <typename T, typename DT = std::decay_t<T>>
	static constexpr bool is_eligible =
		Parent::template is_invocable_v<DT>
		|| Parent::template is_pointer_to_invocable_v<DT>;

	NonCopyablePtrAccept() = default;
	~NonCopyablePtrAccept() = default;
};

#undef _LOBLIBCXX_FTOR_CV_REF
#undef _LOBLIBCXX_FTOR_CV
#undef _LOBLIBCXX_FTOR_REF
#undef _LOBLIBCXX_FTOR_INV_QUALS

///////////////////////////////////////////////////////

