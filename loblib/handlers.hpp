#ifndef LOBLIB_DELEGATE_HANDLERS
#define LOBLIB_DELEGATE_HANDLERS

#include <cstddef>
#include <type_traits>
#include <utility>
#include <functional>

#include "utils.hpp"
#include "object_manager/object_manager.hpp"

///////////////////////////////////////////////////////

namespace loblib::delegate
{

namespace internal
{

template <typename Signature>
class SimpleHandlerVTable;

template <typename Signature>
class DestructibleHandlerVTable;

template <typename Signature>
class CloneableHandlerVTable;

template 
<
	typename Signature, 
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator
>
class VTableHandler;

///////////////////////////////////////////////////////

#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "handlers_impl.hpp"

} // namespace internal
} // namespace loblib::delegate

///////////////////////////////////////////////////////

#endif // !LOBLIB_DELEGATE_HANDLERS
